/* eslint-disable */
export default {
    loadingData: false,
    total: 0,
    page: 1,
    page_size: 3,
    pasiens: [{
            no_lab: "1234",
            tgl_pemeriksaan: "29-09-1996",
            no_rm: "4253",
            nama: "sutejo",
            no_nota: "100090098",
            status_pemeriksaan: "belum diperiksa",
            status_verifikasi: "belum verifikasi"
        },
        {
            no_lab: "1235",
            tgl_pemeriksaan: "08-10-1986",
            no_rm: "4253",
            nama: "supali",
            no_nota: "100090099",
            status_pemeriksaan: "diperiksa",
            status_verifikasi: "belum verifikasi"
        },
        {
            no_lab: "1236",
            tgl_pemeriksaan: "18-04-1979",
            no_rm: "4253",
            nama: "retno",
            no_nota: "100090100",
            status_pemeriksaan: "selesai",
            status_verifikasi: "belum verifikasi"
        },
        {
            no_lab: "1237",
            tgl_pemeriksaan: "05-02-1999",
            no_rm: "4254",
            nama: "jonas",
            no_nota: "100090101",
            status_pemeriksaan: "selesai",
            status_verifikasi: "verifikasi"
        },
        {
            no_lab: "1238",
            tgl_pemeriksaan: "05-02-1980",
            no_rm: "4255",
            nama: "rani",
            no_nota: "100090102",
            status_pemeriksaan: "expertise",
            status_verifikasi: "verifikasi"
        }
    ]
};