/* eslint-disable */
export default {
    SET_VISIBLE_DETAIL(state, value) {
        state.dialogDetailVisible = value;
        console.log("detail status : " + state.dialogDetailVisible)
    },
    SET_DETAIL(state, value) {
        console.log("value set detail :" + value)
        state.id = value.id;
        state.nama = value.nama;
        state.jenis_kelamin = value.jenis_kelamin;
        state.tanggal_lahir = value.tanggal_lahir;
        state.keterangan = value.keterangan;
        state.no_telphone = value.no_telphone;
        state.alamat = value.alamat;
    },
    id(state, value) {
        state.id = value;
    },
    no_rm(state, value) {
        state.no_rm = value;
    },
    nama(state, value) {
        state.nama = value;
    },
    jenis_kelamin(state, value) {
        state.jenis_kelamin = value;
    },
    tanggal_lahir(state, value) {
        state.tanggal_lahir = value;
    },
    no_telphone(state, value) {
        state.no_telphone = value;
    },
    alamat(state, value) {
        state.alamat = value;
    },
    // setDetail(state, row) {
    //     state.id = row.id;
    //     state.alamat = row.alamat;
    //     state.nama = row.nama;
    //     state.jenis_kelamin = row.jenis_kelamin;
    //     state.no_rm = row.no_rm;
    //     state.tanggal_lahir = row.tanggal_lahir;
    //     state.no_telphone = row.no_telphone;
    // }
};